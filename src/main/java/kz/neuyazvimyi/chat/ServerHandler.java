package kz.neuyazvimyi.chat;

import java.util.List;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import kz.neuyazvimyi.proto.Rpc.WirePayload;

@Sharable
public class ServerHandler extends MessageToMessageDecoder<WirePayload> {

	@Override
	protected void decode(ChannelHandlerContext ctx, WirePayload msg, List<Object> out) throws Exception {
		// TODO Auto-generated method stub
		if (msg.hasRpcRequest()) {
			ExecutorService.submit(new PayloadWorker(ctx, msg));
		} else {
			out.add(msg);
			ctx.close();
		}
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		// TODO Auto-generated method stub
		ctx.writeAndFlush(WirePayload.getDefaultInstance());
		ctx.close();
	}

}
