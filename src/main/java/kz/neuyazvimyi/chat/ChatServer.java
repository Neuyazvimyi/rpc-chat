package kz.neuyazvimyi.chat;

import java.sql.SQLException;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class ChatServer {
	
	private ServerBootstrap bootstrap;
	private EventLoopGroup workerGroup;
	private EventLoopGroup bossGroup;
	private ChannelHandler pipelineFactory;
	
	private final int port = 5656;
	private final int backlog = 128;
	private final int bThreadNum = 2;
	private final int wThreadNum = 8;
	private final int buf_size = 1048576;
	private final boolean noDelay = true;
	private final boolean reUseAddr = true;
	private final boolean keepAlive = true;
	private final int numberThreads = 8;
	
	private Channel channel;

	public ChatServer() throws Exception {
		// TODO Auto-generated constructor stub
		initRedis();
		initDBPoolService ();
		initExecutorService();
		initWorkGroup();
		initBootstrap();
		startBootstrap();
	}
	
	private void initBootstrap () {
		bootstrap = new ServerBootstrap();
		bootstrap.group(bossGroup, workerGroup);
		bootstrap.channel(NioServerSocketChannel.class);
		bootstrap.childHandler(pipelineFactory);
		bootstrap.option(ChannelOption.SO_BACKLOG, backlog);
		bootstrap.option(ChannelOption.SO_REUSEADDR, reUseAddr);
		bootstrap.childOption(ChannelOption.SO_KEEPALIVE, keepAlive);
		bootstrap.childOption(ChannelOption.TCP_NODELAY, noDelay);
		bootstrap.childOption(ChannelOption.SO_SNDBUF, buf_size);
        bootstrap.childOption(ChannelOption.SO_RCVBUF, buf_size);
        bootstrap.childOption(ChannelOption.SO_RCVBUF, buf_size);
        bootstrap.childOption(ChannelOption.SO_SNDBUF, buf_size);
	}
	
	private void initDBPoolService () throws SQLException {
		DBPoolService.initDatabasePool();
		DBService dbService = new DBService();
		dbService.initializeDB();
		dbService.close();
	}
	
	private void initRedis () throws SQLException {
		RedisService.initializeRedis();
	}
	
	private void initExecutorService () {
		ExecutorService.initialize(numberThreads);
	}
	
	private void initWorkGroup () throws Exception {
		bossGroup = new NioEventLoopGroup(bThreadNum);
		workerGroup = new NioEventLoopGroup(wThreadNum);
		
		pipelineFactory = new ServerPipelineFactory();
	}
	
	private void startBootstrap () throws Exception {
		try {
			channel = bootstrap.bind(port).sync().channel();
			channel.closeFuture().sync();
		} finally {
			ExecutorService.shutdown();
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}

}
