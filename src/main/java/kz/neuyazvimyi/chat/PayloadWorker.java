package kz.neuyazvimyi.chat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandlerContext;
import kz.neuyazvimyi.proto.Auth.AuthRequest;
import kz.neuyazvimyi.proto.Auth.AuthResponse;
import kz.neuyazvimyi.proto.ChatData.ChatDataRequest;
import kz.neuyazvimyi.proto.ChatData.ChatDataResponse;
import kz.neuyazvimyi.proto.Message.MessageRequest;
import kz.neuyazvimyi.proto.Message.MessageResponse;
import kz.neuyazvimyi.proto.Rpc.RpcRequest;
import kz.neuyazvimyi.proto.Rpc.RpcResponse;
import kz.neuyazvimyi.proto.Rpc.WirePayload;

public class PayloadWorker implements Runnable {
	
	private final Logger logger = LoggerFactory.getLogger(PayloadWorker.class);
	
	private ChannelHandlerContext ctx;
	private WirePayload payload;
	
	public PayloadWorker (ChannelHandlerContext ctx, WirePayload payload) {
		// TODO Auto-generated constructor stub
		this.ctx = ctx;
		this.payload = payload;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if (payload != null && ctx != null) {
			try {
				processPayloadRequest();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(e.getMessage());
				processPayloadResponse(RpcResponse.getDefaultInstance());
			}
		}
	}
	
	private void processPayloadRequest () throws Exception {
		RpcRequest rpcRequest = payload.getRpcRequest();
		RpcResponse rpcResponse = processRpcRequest(rpcRequest);
		
		processPayloadResponse(rpcResponse);
	}
	
	private void processPayloadResponse (RpcResponse rpcResponse) {
		WirePayload.Builder payloadBuilder = WirePayload.newBuilder();
		payloadBuilder.setRpcResponse(rpcResponse);
		
		payload = payloadBuilder.build();
		ctx.writeAndFlush(payload);
		ctx.close();
	}
	
	private RpcResponse processRpcRequest (RpcRequest rpcRequest) throws Exception {
		ChatService chatService = new ChatService();
		
		switch (rpcRequest.getMessageIdentifier()) {
		case ShareData.MSG_ID_AUTH:
			AuthRequest authRequest = AuthRequest.parseFrom(rpcRequest.getRequestBytes());
			AuthResponse authResponse = chatService.processAuthRequest(authRequest);
			return createRpcResponse(authResponse);
		case ShareData.MSG_ID_CHAT_DATA:
			ChatDataRequest chatRequest = ChatDataRequest.parseFrom(rpcRequest.getRequestBytes());
			ChatDataResponse chatResponse = chatService.processChatRequest(chatRequest, rpcRequest.getAccessToken());
			return createRpcResponse(chatResponse);
		case ShareData.MSG_ID_MESSAGE:
			MessageRequest messageRequest = MessageRequest.parseFrom(rpcRequest.getRequestBytes());
			MessageResponse messageResponse = chatService.processMessageRequest(messageRequest, rpcRequest.getAccessToken());
			return createRpcResponse(messageResponse);
		}
		
		return RpcResponse.getDefaultInstance();
	}
	
	private RpcResponse createRpcResponse (AuthResponse response) {
		RpcResponse.Builder builder = RpcResponse.newBuilder();
		builder.setMessageIdentifier(ShareData.MSG_ID_AUTH);
		builder.setResponseBytes(response.toByteString());
		
		return builder.build();
	}
	
	private RpcResponse createRpcResponse (ChatDataResponse response) {
		RpcResponse.Builder builder = RpcResponse.newBuilder();
		builder.setMessageIdentifier(ShareData.MSG_ID_CHAT_DATA);
		builder.setResponseBytes(response.toByteString());
		
		return builder.build();
	}
	
	private RpcResponse createRpcResponse (MessageResponse response) {
		RpcResponse.Builder builder = RpcResponse.newBuilder();
		builder.setMessageIdentifier(ShareData.MSG_ID_MESSAGE);
		builder.setResponseBytes(response.toByteString());
		
		return builder.build();
	}

}
