package kz.neuyazvimyi.chat;

public class Handler {
	public static final String FRAME_DECODER = "frameDecoder";
	public static final String FRAME_ENCODER = "frameEncoder";
	public static final String PROTOBUF_DECODER = "protobufDecoder";
	public static final String PROTOBUF_ENCODER = "protobufEncoder";
	
	public static final String SERVER_CONNECT = "serverConnect";
}
