package kz.neuyazvimyi.chat;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.UUID;

import kz.neuyazvimyi.proto.Auth.AuthRequest;
import kz.neuyazvimyi.proto.Auth.AuthResponse;
import kz.neuyazvimyi.proto.ChatData.ChatDataRequest;
import kz.neuyazvimyi.proto.ChatData.ChatDataResponse;
import kz.neuyazvimyi.proto.ChatData.ChatHistory;
import kz.neuyazvimyi.proto.ChatData.ChatOnlineUsers;
import kz.neuyazvimyi.proto.Message.MessageRequest;
import kz.neuyazvimyi.proto.Message.MessageResponse;

public class ChatService {
	
	public AuthResponse processAuthRequest (AuthRequest request) throws Exception {
		AuthResponse response;
		
		if (request.getCommId() == ShareData.COMM_ID_REGISTER) {
			String userId = request.getUserId();
			String userPass = request.getUserPass();
			
			if (userId != null && userPass != null) {
				DBService dbService = new DBService();
				
				if (!dbService.checkUser(userId)) {
					dbService.addUser(userId, userPass);
					String accessToken = generateAccessToken(userId);
					RedisService.setUserToken(userId, accessToken);
					response = createAuthResponse(ShareData.CODE_ID_SUCCESS, accessToken);
				} else {
					response = createAuthResponse(ShareData.CODE_ID_FAIL, "");
				}
				dbService.close();
				return response;
			}
		} else if (request.getCommId() == ShareData.COMM_ID_LOGIN) {
			String userId = request.getUserId();
			String userPass = request.getUserPass();
			
			if (userId != null && userPass != null) {
				DBService dbService = new DBService();
				if (dbService.authUser(userId, userPass)) {
					String accessToken = generateAccessToken(userId);
					RedisService.setUserToken(userId, accessToken);
					response = createAuthResponse(ShareData.CODE_ID_SUCCESS, accessToken);
				}
				else {
					response = createAuthResponse(ShareData.CODE_ID_FAIL, "");
				}
				dbService.close();
				return response;
			}
		} else if (request.getCommId() == ShareData.COMM_ID_LOG_OUT) {
			String userId = request.getUserId();
			String accessToken = request.getUserPass();
			
			if (userId != null && accessToken != null) {
				if (validateAccessToken(userId, accessToken)) {
					RedisService.removeUser(userId);
					response = createAuthResponse(ShareData.CODE_ID_SUCCESS, "");
				}
				else {
					response = createAuthResponse(ShareData.CODE_ID_FAIL, "");
				}
			}
		}
		return AuthResponse.getDefaultInstance();
	}
	
	public ChatDataResponse processChatRequest (ChatDataRequest request, String accessToken) throws Exception {
		String userId = request.getUserId();
		
		if (userId != null) {
			if (validateAccessToken(userId, accessToken)) {
				if (request.getCommId() == ShareData.COMM_ID_GET_ONLINE_USERS) {
					ChatOnlineUsers.Builder builder = ChatOnlineUsers.newBuilder();
					builder.addAllUserId(RedisService.getUsers());
					return createChatResponse(ShareData.COMM_ID_GET_ONLINE_USERS, builder.build());
				} else if (request.getCommId() == ShareData.COMM_ID_GET_CHAT_HISTORY) {
					DBService dbService = new DBService();
					ArrayList<ChatMessage> chatHIstory = dbService.getChatHistory(25);
					ChatHistory.Builder builder = ChatHistory.newBuilder();
					for (int i=0; i<chatHIstory.size(); i++) {
						ChatMessage chatMsg = chatHIstory.get(i);
						builder.addSenderId(chatMsg.senderId);
						builder.addReceiverId(chatMsg.receiverId);
						builder.addMessage(chatMsg.message);
						builder.addTime(chatMsg.time);
					}
					dbService.close();
					return createChatResponse(ShareData.COMM_ID_GET_CHAT_HISTORY, builder.build());
				}
			}
		}
		
		return ChatDataResponse.getDefaultInstance();
	}
	
	public MessageResponse processMessageRequest (MessageRequest request, String accessToken) throws Exception {
		String userId = request.getUserId();
		String receiverId = request.getReceiverId();
		String data = request.getData();
		
		if (userId != null && data != null) {
			if (validateAccessToken(userId, accessToken)) {
				if (!receiverId.equals("All")) {
					if (isUserOnline(receiverId)) {
						DBService dbService = new DBService();
						String messageId = generateId();
						long time = ExecutorService.getLongTime();
						dbService.addChatMessage(messageId, userId, receiverId, data, time);
						dbService.close();
						return createMessageResponse (ShareData.CODE_ID_SUCCESS);
					}
				}
				else {
					DBService dbService = new DBService();
					String messageId = generateId();
					long time = ExecutorService.getLongTime();
					dbService.addChatMessage(messageId, userId, receiverId, data, time);
					dbService.close();
					return createMessageResponse (ShareData.CODE_ID_SUCCESS);
				}
			}
		}
		return createMessageResponse (ShareData.CODE_ID_FAIL);
	}
	
	private AuthResponse createAuthResponse (int codeId, String token) {
		AuthResponse.Builder builder = AuthResponse.newBuilder();
		builder.setCodeId(codeId);
		builder.setToken(token);
		return builder.build();
	}
	
	private ChatDataResponse createChatResponse (int codeId, ChatOnlineUsers data) {
		ChatDataResponse.Builder builder = ChatDataResponse.newBuilder();
		builder.setCodeId(codeId);
		builder.setOnlineUsers(data);
		return builder.build();
	}
	
	private ChatDataResponse createChatResponse (int codeId, ChatHistory data) {
		ChatDataResponse.Builder builder = ChatDataResponse.newBuilder();
		builder.setCodeId(codeId);
		builder.setChatHistory(data);
		return builder.build();
	}
	
	private MessageResponse createMessageResponse (int codeId) {
		MessageResponse.Builder builder = MessageResponse.newBuilder();
		builder.setCodeId(codeId);
		return builder.build();
	}
	
	private String generateNumber () throws Exception {
		SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
		prng.setSeed(77479974937L);

		return String.valueOf(prng.nextLong());
	}
	
	private String generateAccessToken (String userId) throws Exception {
		String keySource = userId + ExecutorService.getLongTime() + generateNumber();
		String accessToken = Base64.getEncoder().encodeToString(keySource.getBytes());
		
		return accessToken;
	}
	
	private String generateId () {
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	private boolean validateAccessToken (String userId, String accessToken) {
		String cachedToken = RedisService.getUserToken(userId);
		
		if (accessToken != null && cachedToken != null) {
			if (accessToken.equals(cachedToken)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isUserOnline (String userId) {
		String cachedToken = RedisService.getUserToken(userId);
		
		if (cachedToken != null) {
			return true;
		}
		return false;
	}

}
