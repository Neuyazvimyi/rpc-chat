package kz.neuyazvimyi.chat;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariDataSource;

public class DBPoolService {
    
    private final static String POOL_JDBC_NAME = "org.h2.Driver";
	private final static String POOL_NAME = "DBPool";
	
	private final static String DB_URL = "jdbc:h2:~/rpc-chat";
	private final static String LOGIN = "sa";
	private final static String PASSWORD = "";
	
	private final static int MIN_POOL = 4;
	private final static int MAX_POOL = 8;
	private final static int CONN_TIME_OUT = 10000;
	private final static int IDLE_TIME_OUT = 600000;
	private final static int MAX_LIFE_TIME = 1800000;
    
    private static HikariDataSource dataSource;
    
    public static void initDatabasePool () {
    		dataSource = new HikariDataSource();
    		dataSource.setJdbcUrl(DB_URL);
    		dataSource.setUsername(LOGIN);
    		dataSource.setPassword(PASSWORD);
    		dataSource.setDriverClassName(POOL_JDBC_NAME);
    		dataSource.setConnectionTimeout(CONN_TIME_OUT);
    		dataSource.setIdleTimeout(IDLE_TIME_OUT);
    		dataSource.setMaxLifetime(MAX_LIFE_TIME);
    		dataSource.setMinimumIdle(MIN_POOL);
    		dataSource.setMaximumPoolSize(MAX_POOL);
    		dataSource.setPoolName(POOL_NAME);
    		dataSource.setIsolateInternalQueries(true);
	}
    
    public static Connection getConnnectionDB () throws SQLException {
		try {
			return dataSource.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			dataSource.close();
			initDatabasePool();
			
			return dataSource.getConnection();
		}
	}

}
