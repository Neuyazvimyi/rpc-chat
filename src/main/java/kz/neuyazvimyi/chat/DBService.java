package kz.neuyazvimyi.chat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBService {
	
	private Connection conn;

	public DBService() throws SQLException {
		conn = DBPoolService.getConnnectionDB();
	}
	
	public void initializeDB () throws SQLException {
		String sqlUsers = "CREATE TABLE IF NOT EXISTS users (" + 
				"userId	VARCHAR(255) NOT NULL," + 
				"userPass VARCHAR(255) NOT NULL" + 
				");";
		
		String sqlMessages = "CREATE TABLE IF NOT EXISTS messages (" + 
				"messageId VARCHAR(255) NOT NULL," + 
				"senderId VARCHAR(255) NOT NULL," + 
				"receiverId VARCHAR(255) NOT NULL," + 
				"message	 VARCHAR(255) NOT NULL," + 
				"time BIGINT NOT NULL" + 
				");";
		
		Statement st = conn.createStatement();
        st.executeUpdate(sqlUsers);
        st.executeUpdate(sqlMessages);
        st.close();
	}
	
	public boolean checkUser (String userId) throws SQLException {
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT userId FROM users WHERE userId='"+userId+"' ;");
		
		return rs.next();
	}
	
	public void addUser (String userId, String userPass) throws SQLException {
		Statement st = conn.createStatement();
		st.execute("INSERT INTO users VALUES ('"+userId+"', '"+userPass+"') ;");
	}
	
	public boolean authUser (String userId, String userPass) throws SQLException {
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT userId FROM users WHERE userId='"+userId+"' AND userPass='"+userPass+"' ;");
		
		return rs.next();
	}
	
	public void addChatMessage (String messageId, String senderId, String receiverId, String message, long time) throws SQLException {
		Statement st = conn.createStatement();
		st.execute("INSERT INTO messages VALUES ('"+messageId+"', '"+senderId+"', '"+receiverId+"', '"+message+"', "+time+") ;");
	}
	
	public ArrayList<ChatMessage> getChatHistory (int limit) throws SQLException {
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("SELECT senderId,receiverId,message,time FROM messages ORDER BY time DESC LIMIT "+limit+" ;");
		
		ArrayList<ChatMessage> chatHistory = new ArrayList<ChatMessage>();
		while (rs.next()) {
			ChatMessage chatMsg = new ChatMessage();
			chatMsg.senderId = rs.getString(1);
			chatMsg.receiverId = rs.getString(2);
			chatMsg.message = rs.getString(3);
			chatMsg.time = rs.getLong(4);
			chatHistory.add(chatMsg);
		}
		return chatHistory;
	}
	
	public void close () throws SQLException {
		conn.close();
	}

}
