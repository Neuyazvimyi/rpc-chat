package kz.neuyazvimyi.chat;

import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;

public class ExecutorService {
	
	private static EventExecutorGroup executorGroup;
	
	public static void initialize (int nThreads) {
		executorGroup = new DefaultEventExecutorGroup(nThreads);
	}
	
	public static void submit (Runnable task) {
		executorGroup.submit(task);
	}
	
	public static void shutdown () {
		executorGroup.shutdownGracefully();
	}
	
	public static long getLongTime () {
		return System.currentTimeMillis();
	}
	
}
