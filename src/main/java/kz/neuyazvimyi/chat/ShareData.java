package kz.neuyazvimyi.chat;

public class ShareData {
	
	public static final int MSG_ID_AUTH = 10001;
	public static final int MSG_ID_CHAT_DATA = 10002;
	public static final int MSG_ID_MESSAGE = 10003;
	
	public static final int COMM_ID_REGISTER = 20001;
	public static final int COMM_ID_LOGIN = 20002;
	public static final int COMM_ID_LOG_OUT = 20003;
	
	public static final int COMM_ID_GET_ONLINE_USERS = 30001;
	public static final int COMM_ID_GET_USER_CHATS = 30002;
	public static final int COMM_ID_GET_CHAT_HISTORY = 30003;
	
	public static final int CODE_ID_SUCCESS = 100;
	public static final int CODE_ID_FAIL = 101;
	public static final int CODE_ID_ERROR = 102;

}
