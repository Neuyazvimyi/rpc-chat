package kz.neuyazvimyi.chat;

import java.util.ArrayList;

import redis.clients.jedis.Jedis;

public class RedisService {
	
	private final static String REDIS_URL = "127.0.0.1";
	private final static int REDIS_PORT = 6379;
	
	private static Jedis jedis;

	public static void initializeRedis () {
		jedis = new Jedis(REDIS_URL, REDIS_PORT);
	}
	
	public static void setUserToken (String userId, String accessToken) {
		jedis.set(userId, accessToken);
	}
	
	public static String getUserToken (String userId) {
		return jedis.get(userId);
	}
	
	public static void removeUser (String userId) {
		jedis.del(userId);
	}
	
	public static ArrayList<String> getUsers () {
		return (ArrayList<String>) jedis.keys("*");
	}

}
